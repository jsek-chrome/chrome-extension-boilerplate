<p align="center">
  <a href="https://gitlab.com/jsek-chrome/chrome-extension-boilerplate">
    <img height="128" width="128" src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/2102927/icon-256.png">
  </a>
  <h1 align="center">Google Chrome Extension boilerplate</h1>
</p>

---

## Checklist

After forking or cloning the repo follow these steps:

- [ ] Readme.md: Update title, description, remove this checklist
- [ ] package.json: name, author, repository url
- [ ] manifest.json: name, description, browser\_action default\_title
- [ ] popup.pug: repository url
- [ ] Choose new icons from [Flaticon](http://www.flaticon.com)
- [ ] Implement first version of your new extension for Chrome

## Setup

```bash
npm install
```

### Build

```bash
npm run build
```

### Pack before publish

1. Generate `key.pem` once using `npm run keygen` command

Create `*.crx` and `*.zip` packages

```bash
gulp pack
```

## Credits

Icons made by [Freepik](http://www.freepik.com) from [Flaticon](http://www.flaticon.com) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)